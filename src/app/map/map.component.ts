import { Component, OnInit } from '@angular/core';
import * as Leaflet from 'leaflet';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  title = 'leafletApps';
  map: Leaflet.Map;

  ngOnInit() {
    this.map = Leaflet.map('map').setView([47.0701713, 28.6835753], 15);
    Leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: 'edupala.com © Angular LeafLet',
    }).addTo(this.map);

    Leaflet.marker([47.0701713, 28.6835753]).addTo(this.map).bindPopup('Ne Puteți găsi aici').openPopup();
  }
}
