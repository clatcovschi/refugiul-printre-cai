import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { SlickCarouselModule } from 'ngx-slick-carousel';

import { AppComponent } from './app.component';
import { FetchJsonPipe } from './fetch-json.pipe';
import { MainComponent } from './main/main.component';
import {NavComponent} from './nav/nav.component';
import { ServiciileNoastreComponent } from './serviciile-noastre/serviciile-noastre.component';
import { FotosesiiComponent } from './fotosesii/fotosesii.component';
import { SubscriptionsComponent } from './subscriptions/subscriptions.component';
import { MapComponent } from './map/map.component';
import { WalksComponent } from './walks/walks.component';
import { PromotionComponent } from './promotion/promotion.component';
import { FooterComponent } from './footer/footer.component';
import { ContactComponent } from './contact/contact.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthorComponent } from './author/author.component';
@NgModule({
  declarations: [
    AppComponent,
    FetchJsonPipe,
    MainComponent,
    NavComponent,
    ServiciileNoastreComponent,
    FotosesiiComponent,
    SubscriptionsComponent,
    MapComponent,
    WalksComponent,
    PromotionComponent,
    FooterComponent,
    ContactComponent,
    AuthorComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule,
    SlickCarouselModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
